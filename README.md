## Initial setup for a brand new server

1. Clone this repo;
1. Make sure you have the required dev tools installed:
	1. nodejs:
		1. Install [fnm](https://github.com/Schniz/fnm);
		1. Run `fnm use` on the root of this project and allow the installer to proceed with the installation;
		1. Run `npm i`.
	1. [docker](https://docs.docker.com/engine/install/);
	1. [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) (good luck!).
1. Create an account in aws;
1. Create initial resources in aws (more detailed instruction will be added in the future):
	1. [Parameter Store](https://eu-west-1.console.aws.amazon.com/systems-manager/parameters/?region=eu-west-1&tab=Table):
		1. `/akaunting/app/admin-password` with the same value from the original account;
		1. `/akaunting/db/admin-password` with the same value from the original account.
	1. [EC2 Key Pair](https://eu-west-1.console.aws.amazon.com/ec2/v2/home?region=eu-west-1#KeyPairs:):
		1. `akaunting-ec2-key` save the key as `akaunting-ec2-key.pem` in the `ansible` folder.
1. Generate access keys for you account:
	1. From AWS UI go to: Account -> Security Credentials -> Access Keys -> and copy the keys;
	1. Setup a local account named `cisma1` with the copied keys and `eu-west-1` region using `aws configure` [docs](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html#cli-configure-files-methods).

## Infrastructure and server setup

1. Follow the steps described in the [Initial setup](#initial-setup) once;
2. Run `npm run deploy`;
3. Wait until the job finishes and displays the EC2's IP address;
4. Copy the EC2 IP and replace the existing DNS record value in [cpanel](https://cpanel.cismacultural.pt/cpsess1309757243/frontend/paper_lantern/zone_editor/index.html?l#/manage?domain=cismacultural.pt) for the `finances.cismacultural.pt` entry with the copied value;
5. Run `npm run setup-server`;
	1. If you get an error saying that the ip is different from what is expected, wait a few minutes and try again. The new record is still propagating;
6. ...
7. Profit!

Don't forget to copy the old DB data and destroy the resources in the old account.

## Options

### Rerunning specific jobs

The ansible playbook is split into multiple jobs. You can run a specific set of jobs by adding `--tags "your_tags_here"` to the last `RUN` instruction in `Dockerfile` and rerunning the `setup-server` script.

Here's the list of tags that can be used:

1. `"packages"` - installs the required packages;
1. `"akaunting-app"` - only installs the akaunting app;
1. `"certificate`" - installs certbot, requests a new certificate and installs the automatic renewal service;
1. `"certificate-renewal`" - installs the automatic renewal service only;
1. `"observability"` - install observability tools to add more metrics to cloudwatch;

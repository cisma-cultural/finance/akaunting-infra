cd /home/ec2-user/akaunting-container-setup

export AK_APP_PASSWORD=$(aws ssm get-parameter --name /akaunting/app/admin-password --region eu-west-1 --with-decryption | jq -r '.Parameter.Value')
export AK_DB_PASSWORD=$(aws ssm get-parameter --name /akaunting/db/admin-password --region eu-west-1 --with-decryption | jq -r '.Parameter.Value')
export APP_URL=https://finances.cismacultural.pt
export AWS_REGION='eu-west-1'
export CLOUDWATCH_AK_APP_GROUP_NAME='/aws/ec2/akaunting/app'
export CLOUDWATCH_AK_DB_GROUP_NAME='/aws/ec2/akaunting/db'

./start.sh

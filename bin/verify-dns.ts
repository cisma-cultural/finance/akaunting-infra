import { lookup } from "dns";
import { exit } from "process";
import { AkauntingStack } from "../outputs.json"

lookup('finances.cismacultural.pt', (err, address) => {
  if (err) {
    console.error('Unknown error!');
    console.error(err);
    exit(1);
  }
  if (address !== AkauntingStack.ec2ip) {
    console.error(`Current ip ${address} is different from expected ${AkauntingStack.ec2ip}.`);
    console.error(`Make sure you've updated the DNS record for finances.cismacultural.pt to point to ${AkauntingStack.ec2ip}.`);
    exit(1);
  }

  exit(0);
});

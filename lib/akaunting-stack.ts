import * as ec2 from 'aws-cdk-lib/aws-ec2';
import * as cdk from 'aws-cdk-lib';
import {CfnEIP} from 'aws-cdk-lib/aws-ec2';
import {CfnOutput} from 'aws-cdk-lib';
import {StringParameter} from 'aws-cdk-lib/aws-ssm';
import {LogGroup, RetentionDays} from 'aws-cdk-lib/aws-logs';
import {ManagedPolicy, Policy} from 'aws-cdk-lib/aws-iam';

export class AkauntingStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const vpc = ec2.Vpc.fromLookup(this, 'my-cdk-vpc', {isDefault: true});

    const ec2Instance = this.createEC2Resources(vpc);

    this.createEC2Logs(ec2Instance);

    this.setupEC2Secrets(ec2Instance);

    new CfnOutput(this, 'ec2-ip', {
      value: ec2Instance.instancePublicIp
    });
  }

  private setupEC2Secrets(ec2Instance: ec2.Instance) {
    const akauntingDBAdminPassword = StringParameter.fromSecureStringParameterAttributes(this, 'db-admin-password', {parameterName: '/akaunting/db/admin-password'});
    akauntingDBAdminPassword.grantRead(ec2Instance);

    const akauntingAppAdminPassword = StringParameter.fromSecureStringParameterAttributes(this, 'app-admin-password', {parameterName: '/akaunting/app/admin-password'});
    akauntingAppAdminPassword.grantRead(ec2Instance);
  }

  private createEC2Resources(vpc: ec2.IVpc) {
    const webserverSG = new ec2.SecurityGroup(this, 'webserver-sg', {
      vpc,
      allowAllOutbound: true,
    });

    webserverSG.addIngressRule(
      ec2.Peer.anyIpv4(),
      ec2.Port.tcp(22),
      'allow SSH access from anywhere'
    );

    webserverSG.addIngressRule(
      ec2.Peer.anyIpv4(),
      ec2.Port.tcp(80),
      'allow HTTP traffic from anywhere'
    );

    webserverSG.addIngressRule(
      ec2.Peer.anyIpv4(),
      ec2.Port.tcp(443),
      'allow HTTPS traffic from anywhere'
    );

    const ec2Instance = new ec2.Instance(this, 'ec2-instance', {
      vpc,
      vpcSubnets: {
        subnetType: ec2.SubnetType.PUBLIC,
      },
      securityGroup: webserverSG,
      instanceType: ec2.InstanceType.of(
        ec2.InstanceClass.BURSTABLE2,
        ec2.InstanceSize.MICRO
      ),
      machineImage: new ec2.AmazonLinuxImage({
        generation: ec2.AmazonLinuxGeneration.AMAZON_LINUX_2,
      }),
      keyName: 'akaunting-ec2-key',
    });

    new CfnEIP(this, 'ic2-ip', {
      domain: 'vpc',
      instanceId: ec2Instance.instanceId
    });
    return ec2Instance;
  }

  private createEC2Logs(ec2Instance: ec2.Instance) {
    const ec2AkauntingAppLogGroup = new LogGroup(this, 'akaunting-app-logs', {
      logGroupName: '/aws/ec2/akaunting/app',
      retention: RetentionDays.ONE_MONTH
    });
    ec2AkauntingAppLogGroup.grantWrite(ec2Instance);

    const ec2AkauntingDBLogGroup = new LogGroup(this, 'akaunting-db-logs', {
      logGroupName: '/aws/ec2/akaunting/db',
      retention: RetentionDays.ONE_MONTH
    });
    ec2AkauntingDBLogGroup.grantWrite(ec2Instance);

    const ec2AkauntingNginxLogGroup = new LogGroup(this, 'akaunting-nginx-access-logs', {
      logGroupName: '/aws/ec2/akaunting/nginx/access',
      retention: RetentionDays.ONE_MONTH
    });
    ec2AkauntingNginxLogGroup.grantWrite(ec2Instance);
    
    ec2Instance.role.addManagedPolicy(ManagedPolicy.fromAwsManagedPolicyName('CloudWatchAgentServerPolicy'));
  }
}
